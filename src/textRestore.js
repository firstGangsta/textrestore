//API плагина

// Плагин ПРИНИМАЕТ следующие параметры:
// restoreAnimationTime - время, за которое текст визуально "восстанавливается" (по-умолчанию 0)
// afterDestroy - функция, которая вызовется сразу после того, как текст поломается (по-умолчанию функции нет)
// afterRestore - функция, которая вызовется сразу после того, как текст восстановится (по-умолчанию функции нет)
// destroyRadius - величина разброса букв (по-умолчанию 15 пикселей)

// Плагин ВОЗВРАЩАЕТ объект со следующими полями
// destroy - функция, которая "ломает" текст (функция ничего не принимает и не возвращает)
// restore - функция, которая "восстанавливает" текст (функция ничего не принимает и не возвращает)

(function($) {
  $.fn.textRestore = function(data) {

    var objectWithFunctions = {
      destroyers: [],
      restrotrators: []
    };



    $(this).each(function(index, selector) {
      $(selector).addClass('textRestore');

      //инициализация значений по-умолчанию

      var destroyAnimationTime = 0,
        restoreAnimationTime = 0, destroyRadius = 15,
        text, letters, i, selectorWidth, selectorHeight, lastLetter,
        minX, maxX, minY, maxY, randomOffsetX, randomOffsetY, newHtml, letterHtml;

      function afterDestroy() {
        console.log("textRestore: TEXT DESTROYED");
      }

      function afterRestore() {
        console.log("textRestore: TEXT RESTORED");
      }

      if (data) {
        restoreAnimationTime = data.restoreAnimationTime || restoreAnimationTime;
        afterDestroy = data.afterDestroy || afterDestroy;
        afterRestore = data.afterRestore || afterRestore;
        destroyRadius = data.destroyRadius || destroyRadius;
      }

      if (selector) {
        selectorWidth = $(selector).width();
        selectorHeight = $(selector).height();
      }

      //функция поломки текста
      function destroy() {

        //превращаем весь текст в набор span и заодно запоминаем их координаты

        text = $(selector).text();

        letters = text.split("");

        $(selector).text("");

        newHtml = "";
        minX = -1 * destroyRadius;
        maxX = destroyRadius;
        minY = -1 * destroyRadius;
        maxY = destroyRadius;
        for (i = 0; i < letters.length; i++) {
          letterHtml = "";

          randomOffsetX = getRandomNumber(minX, maxX);
          randomOffsetY = getRandomNumber(minY, maxY);

          if (letters[i] === ' ') {
            letterHtml += "<span class='textRestore__letter' style=' transform:translate(" + randomOffsetX + "px, " + randomOffsetY + "px);'>" + "&nbsp;" + "</span>";
          } else {
            letterHtml += "<span class='textRestore__letter' style=' transform:translate(" + randomOffsetX + "px, " + randomOffsetY + "px);'>" + letters[i] + "</span>"
          }

          newHtml += letterHtml;


        }

        $(selector).append(newHtml)

        //КОЛБЭК
        afterDestroy();
      }


      //функция восстановления текста
      function restore() {
        $(selector).find('.textRestore__letter').css({
          'transform': "none",
          'transition': "transform " + restoreAnimationTime + "ms" + " linear",
        });

        setTimeout(afterRestore, restoreAnimationTime);
        setTimeout(function() {
          $(window).resize()
        }, restoreAnimationTime + 50);
      }

      objectWithFunctions.destroyers.push(destroy);
      objectWithFunctions.restrotrators.push(restore);

    })


    objectWithFunctions.destroy = function() {
      this.destroyers.forEach(function(destroy) {
        destroy();
      })
    };

    objectWithFunctions.restore = function() {
      this.restrotrators.forEach(function(restore) {
        restore();
      })
    }

    return objectWithFunctions;



    //ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ
    function getRandomNumber(min, max) {
      let res = Math.floor(Math.random() * (max - min));
      return res - Math.abs(min);
    }
  };
})(jQuery);
